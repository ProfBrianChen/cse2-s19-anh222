//WelcomeClass HW01

public class WelcomeClass{
  public static void main(String args[]){
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-A--N--H--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    
    System.out.println("My name is Andrew Ha. I am a first-year student at Lehigh Univerity");
  }
}