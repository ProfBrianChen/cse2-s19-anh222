//Andrew Ha
//CSE 002 lab08

import java.util.Arrays;

import com.sun.prism.paint.Stop;

import java.lang.Math;


public class lab08 {

    public static int getRange(int[] integers){
        Arrays.sort(integers);
        int range = integers[integers.length - 1] - integers[0];
        return range;
    }

    public static double getMean(int[] integers){
        int mean = 0;
        for(int i = 0; i < integers.length; i++){
            mean += integers[i];
        }
        mean = mean / integers.length;
        return mean;
    }

    public static double getStdDev(int[] integers){
        int deviation = 0;
        for (int i = 0; i < integers.length; i++){
            deviation += integers[i] - getMean(integers);
        }
        return Math.sqrt(Math.pow(deviation, 2) / (integers.length - 1));
    }

    public static void shuffle(int[] integers){
        for(int i = 0; i < integers.length; i++){
            int random = (int) (Math.random() * (((integers.length - 1) - 0) + 1)) + 0;
            int random2 = (int) (Math.random() * (((integers.length - 1) - 0) + 1)) + 0;
            integers[random] = integers[random2];
        }
    }

    public static void main(String args[]){
        // Random random = new Random();

        int randomNumber = (int)(Math.random() * ((100 - 50) + 1)) + 50;

        
        int[] integers = new int[randomNumber];

        System.out.println(integers.length);
        System.out.println("======================");

        for(int i = 0; i < integers.length; i++){
            int randomNumber2 = (int) (Math.random() * ((99 - 0) + 1)) + 0;
            integers[i] = randomNumber2;
        }

        for(int j = 0; j < integers.length; j++){
            System.out.println(integers[j]);
        }

        System.out.println("========================");

        System.out.println(getMean(integers));

        System.out.println("========================");

        System.out.println(getStdDev(integers));

        System.out.println("========================");

        shuffle(integers);

        for (int j = 0; j < integers.length; j++) {
            System.out.println(integers[j]);
        }

        System.out.println("========================");



    }
}