// Andrew Ha CSE 002 lab07 3/22/19
// The purpose of this program is to use methods to generate a random number to choose a random subject, adjective, verb, or object

import java.util.Random;


public class lab07 {
    public static String adjective() {
        Random random = new Random();
        int randomNumber = random.nextInt(10);
        String adjective = "";

        switch (randomNumber) {
        case 0:
            adjective = "angry";
            break;
        case 1:
            adjective = "sad";
            break;
        case 2:
            adjective = "happy";
            break;
        case 3:
            adjective = "amazing";
            break;
        case 4:
            adjective = "fuzzy";
            break;
        case 5:
            adjective = "slippery";
            break;
        case 6:
            adjective = "loose";
            break;
        case 7:
            adjective = "cold";
            break;
        case 8:
            adjective = "rough";
            break;
        case 9:
            adjective = "ecstatic";
            break;
        }
        return adjective;
    }

    public static String noun() {
        Random random = new Random();
        int randomNumber = random.nextInt(10);
        String noun = "";

        switch (randomNumber) {
        case 0:
            noun = "dog";
            break;
        case 1:
            noun = "cat";
            break;
        case 2:
            noun = "man";
            break;
        case 3:
            noun = "woman";
            break;
        case 4:
            noun = "bear";
            break;
        case 5:
            noun = "merchant";
            break;
        case 6:
            noun = "athlete";
            break;
        case 7:
            noun = "fisherman";
            break;
        case 8:
            noun = "manager";
            break;
        case 9:
            noun = "soccer ball";
        }
        return noun;
    }

    public static String pastTenseVerb() {
        Random random = new Random();
        int randomNumber = random.nextInt(10);
        String pastTenseVerb = "";

        switch (randomNumber) {
        case 0:
            pastTenseVerb = "passed";
            break;
        case 1:
            pastTenseVerb = "walked";
            break;
        case 2:
            pastTenseVerb = "tied";
            break;
        case 3:
            pastTenseVerb = "packed";
            break;
        case 4:
            pastTenseVerb = "stole";
            break;
        case 5:
            pastTenseVerb = "made";
            break;
        case 6:
            pastTenseVerb = "ripped";
            break;
        case 7:
            pastTenseVerb = "purchased";
            break;
        case 8:
            pastTenseVerb = "bamboozled";
            break;
        case 9:
            pastTenseVerb = "robbed";
        }
        return pastTenseVerb;
    }

    public static String noun2() {
        Random random = new Random();
        int randomNumber = random.nextInt(10);
        String noun2 = "";
        
        switch (randomNumber) {
        case 0:
            noun2 = "fox";
            break;
        case 1:
            noun2 = "magician";
            break;
        case 2:
            noun2 = "model";
            break;
        case 3:
            noun2 = "officer";
            break;
        case 4:
            noun2 = "teacher";
            break;
        case 5:
            noun2 = "professor";
            break;
        case 6:
            noun2 = "wizard";
            break;
        case 7:
            noun2 = "policeman";
            break;
        case 8:
            noun2 = "waiter";
            break;
        case 9:
            noun2 = "referee";
            break;
        }
        return noun2;
    }

    public static String thesis() {
        String subject = "";
        for (int i = 1; i <= 8; i++) {
            switch (i) {
            case 1:
                System.out.print("The ");
                break;
            case 2:
                System.out.print(adjective() + " ");
                break;
            case 3:
                System.out.print(adjective() + " ");
                break;
            case 4:
                subject = noun();
                System.out.print(subject + " ");
                break;
            case 5:
                System.out.print(pastTenseVerb() + " ");
                break;
            case 6:
                System.out.print("the ");
                break;
            case 7:
                System.out.print(adjective() + " ");
                break;
            case 8:
                System.out.println(noun() + ". ");
                break;
            }
        }
        return subject;
    }

    public static String action(String subject) {
        Random random = new Random();
        int randomNumber = random.nextInt(2);

        for (int i = 1; i <= 8; i++) {
            switch (i) {
            case 1:
                if (randomNumber == 0){
                    System.out.print("It ");
                }
                else {
                    System.out.print("This " + subject + " ");
                }
                break;
            case 2:
                System.out.print("used a ");
                break;
            case 3:
                System.out.print(noun() + " and ");
                break;
            case 5:
                System.out.print(pastTenseVerb() + " ");
                break;
            case 6:
                System.out.print("the ");
                break;
            case 7:
                System.out.print(adjective() + " ");
                break;
            case 8:
                System.out.println(noun() + ".");
                break;
            }
        }
        return subject;
    }

    public static String conclusion(String subject) {

        for (int i = 1; i <= 6; i++) {
            switch (i) {
            case 1:
                System.out.print("The ");
                break;
            case 2:
                System.out.print(subject + " ");
                break;
            case 3:
                System.out.print("eventually ");
                break;
            case 4:
                System.out.print(pastTenseVerb() + " ");
                break;
            case 5:
                System.out.print("his ");
                break;
            case 6:
                System.out.print(noun2() + ".");
                break;
            }
        }
        return subject;
    }

    public static void main(String args[]){
    
        conclusion(action(thesis()));

    }
}
