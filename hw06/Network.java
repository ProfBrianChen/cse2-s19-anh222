//Andrew Ha CSE 002 hw06 3/19/19
//The purpose of this program is to use loops to print out squares of different sizes

import java.util.Scanner;       //importing scanner

public class Network{
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);

        //initialzing variables
        int height;
        int width;
        int squareSize;
        int edgeLength;

        int widthCounter = 1; // counter to keep track of size of the square
        int heightCounter = 1; // iterating through each row
        int edgeCounter = 1; // iterating through edge on side of squares
        int heightEdgeCounter = 1; // iterating through edge on bottom of squares

        while (true) {
            System.out.println("Input your desired height: ");

            boolean isInt = scan.hasNextInt();

            if (isInt) {
                height = scan.nextInt();
                if (height > 0){            //checking if integer is positive
                    break;
                }
                else {
                    System.out.println("Postive integer required");
                }
            } else {
                System.out.println("Integer input required"); //if input is not integer, user is asked to input integer
                String junkword = scan.next(); 
            }
        }

        while (true) {
            System.out.println("Input your desired width: ");

            boolean isInt = scan.hasNextInt();

            if (isInt) {
                width = scan.nextInt();
                if (width > 0) {
                    break;
                } else {
                    System.out.println("Postive integer required");
                }
            } else {
                System.out.println("Integer input required");
                String junkword = scan.next();
            }
        }

        while (true) {
            System.out.println("Input square size: ");

            boolean isInt = scan.hasNextInt();

            if (isInt) {
                squareSize = scan.nextInt();
                if (squareSize > 0) {
                    break;
                } else {
                    System.out.println("Postive integer required");
                }
            } else {
                System.out.println("Integer input required");
                String junkword = scan.next();
            }
        }

        while (true) {
            System.out.println("Input edge length: ");

            boolean isInt = scan.hasNextInt();

            if (isInt) {
                edgeLength = scan.nextInt();
                if (edgeLength > 0) {
                    break;
                } else {
                    System.out.println("Postive integer required");
                }
            } else {
                System.out.println("Integer input required");
                String junkword = scan.next();
            }
        }


        if (squareSize % 2 == 0){                   //printing squares with even size
            for (int i = 1; i <= height; i++) {

                for (int j = 1; j <= width; j++) { // iterating through line
                    if (heightCounter == 1 || heightCounter == squareSize) { // what to print at first and last line of square

                        if (widthCounter == 1) { // first character printed will always be #
                            System.out.print("#");
                            widthCounter++;
                        } else if (widthCounter == squareSize) { // When counter reaches end of squareSize, it will print # then decide whether to print spaces or - depending on which line its in
                            System.out.print("#");
                            widthCounter = 0; // reset counter to 0 to get ready for next square
                        } else if (widthCounter == 0) { // after widthCounter is equal to square size, it is set to 0 so counter can run for edge length
                            if (edgeCounter <= edgeLength) {
                                System.out.print(" ");
                                edgeCounter++;
                            } else { // once edgeCounter reaches edgeLength, widthCounter and edgeCounter is reset to 1 for next square. j is also decremented to cancel the increment at end of loop because nothing is being printed
                                edgeCounter = 1;
                                widthCounter = 1;
                                j--;
                            }
                        } else {
                            System.out.print("-"); // if not printing either # or space, - is printed
                            widthCounter++;
                        }
                    }

                    else if (heightCounter == 0) { // printing vertical edges
                        if (heightEdgeCounter <= edgeLength) {
                            if (widthCounter == 1) {
                                System.out.print(" ");
                                widthCounter++;
                            } else if (widthCounter == squareSize) {
                                System.out.print(" ");
                                widthCounter = 0;
                            } else if (widthCounter == 0) {
                                if (edgeCounter <= edgeLength) {
                                    System.out.print(" ");
                                    edgeCounter++;
                                } else {
                                    edgeCounter = 1;
                                    widthCounter = 1;
                                    j--;
                                }
                            } else {
                                if (widthCounter == (squareSize / 2) || widthCounter == ((squareSize / 2) + 1)) {
                                    System.out.print("|");
                                    widthCounter++;
                                } else {
                                    System.out.print(" ");
                                    widthCounter++;
                                }
                            }

                        }

                        else { // after all edges are printed, counters become reset to be able to print next square
                            edgeCounter = 1;
                            widthCounter = 1;
                            heightEdgeCounter = 1;
                            heightCounter = 1;
                            j--;
                        }
                    }

                    else { // printing sides of square
                        if (widthCounter == 1) {
                            System.out.print("|");
                            widthCounter++;
                        } else if (widthCounter == squareSize) {
                            System.out.print("|");
                            widthCounter = 0;
                        } else if (widthCounter == 0) {
                            if (edgeCounter <= edgeLength) {
                                if (heightCounter == (squareSize / 2) || heightCounter == ((squareSize / 2) + 1)) {
                                    System.out.print("-");

                                } else {
                                    System.out.print(" ");

                                }
                                edgeCounter++;
                            } else {
                                edgeCounter = 1;
                                widthCounter = 1;
                                j--;
                            }
                        } else {
                            System.out.print(" ");
                            widthCounter++;
                        }
                    }
                }

                widthCounter = 1;
                edgeCounter = 1;
                if (heightCounter == squareSize) { // conditional for heightCounter to detect whether program should start printing vertical edges or not
                    heightCounter = 0;
                } else if (heightCounter == 0) {
                    heightEdgeCounter++;
                } else {
                    heightCounter++;
                }

                System.out.println("");
            }
        }
        else if (height < squareSize || width < squareSize) {   //printing squares when width and height is less than square size
            for (int i = 1; i <= width; i++) {
                if (i == 1) {
                    System.out.print("#");
                } else {
                    System.out.print("-");
                }
            }

            System.out.println("");

            for (int j = 1; j <= height; j++) {
                System.out.println("|");
            }
        }
        else if (squareSize == 1) {               //printing squares with square size 1
            for (int i = 1; i <= height; i++) {
                for (int j = 1; j <= width; j++) {
                    if (heightCounter == 1) { // printing single squares and horizontal edges
                        if (widthCounter == 1) {
                            System.out.print("#");
                            widthCounter++;
                        }

                        else if (widthCounter == (edgeLength + 2)) {

                            widthCounter = 1;
                            j--;
                        } else {
                            System.out.print("-");
                            widthCounter++;
                        }
                    }

                    else { // printing vertical edges between squares
                        if (widthCounter == 1) {
                            System.out.print("|");
                            widthCounter++;
                        } else if (widthCounter == (edgeLength + 2)) {
                            widthCounter = 1;
                            j--;
                        } else {
                            System.out.print(" ");
                            widthCounter++;
                        }

                    }
                }
                widthCounter = 1;
                if (heightCounter == (edgeLength + 1)) { // conditional to detect whether vertical or horizontal edges should be printed
                    heightCounter = 1;
                } else {
                    heightCounter++;
                }
                System.out.println("");
            }
        }
        else {                                    //printing squares with odd square size
            for (int i = 1; i <= height; i++) {

                for (int j = 1; j <= width; j++) { // iterating through line
                    if (heightCounter == 1 || heightCounter == squareSize) { // what to print at first and last line of square

                        if (widthCounter == 1) { // first character printed will always be #
                            System.out.print("#");
                            widthCounter++;
                        } else if (widthCounter == squareSize) { // When counter reaches end of squareSize, it will print # then decide whether to print spaces or - depending on which line its in
                            System.out.print("#");
                            widthCounter = 0; // reset counter to 0 to get ready for next square
                        } else if (widthCounter == 0) { // after widthCounter is equal to square size, it is set to 0 so counter can run for edge length
                            if (edgeCounter <= edgeLength) {
                                System.out.print(" ");
                                edgeCounter++;
                            } else {
                                edgeCounter = 1;
                                widthCounter = 1;
                                j--;
                            }
                        } else {
                            System.out.print("-"); // if not printing either # or space, - is printed
                            widthCounter++;
                        }
                    }

                    else if (heightCounter == 0) { // printing vertical edges between squares
                        if (heightEdgeCounter <= edgeLength) {
                            if (widthCounter == 1) {
                                System.out.print(" ");
                                widthCounter++;
                            } else if (widthCounter == squareSize) {
                                System.out.print(" ");
                                widthCounter = 0;
                            } else if (widthCounter == 0) {
                                if (edgeCounter <= edgeLength) {
                                    System.out.print(" ");
                                    edgeCounter++;
                                } else {
                                    edgeCounter = 1;
                                    widthCounter = 1;
                                    j--;
                                }
                            } else {
                                if (widthCounter == ((squareSize / 2) + 1)) {
                                    System.out.print("|");
                                    widthCounter++;
                                } else {
                                    System.out.print(" ");
                                    widthCounter++;
                                }
                            }

                        }

                        else {
                            edgeCounter = 1;
                            widthCounter = 1;
                            heightEdgeCounter = 1;
                            heightCounter = 1;
                            j--;
                        }
                    }

                    else { // printing sides of square
                        if (widthCounter == 1) {
                            System.out.print("|");
                            widthCounter++;
                        } else if (widthCounter == squareSize) {
                            System.out.print("|");
                            widthCounter = 0;
                        } else if (widthCounter == 0) {
                            if (edgeCounter <= edgeLength) {
                                if (heightCounter == ((squareSize / 2) + 1)) {
                                    System.out.print("-");

                                } else {
                                    System.out.print(" ");

                                }
                                edgeCounter++;
                            } else {
                                edgeCounter = 1;
                                widthCounter = 1;
                                j--;
                            }
                        } else {
                            System.out.print(" ");
                            widthCounter++;
                        }
                    }

                }
                widthCounter = 1;
                edgeCounter = 1;
                if (heightCounter == squareSize) {
                    heightCounter = 0;
                } else if (heightCounter == 0) {
                    heightEdgeCounter++;
                } else {
                    heightCounter++;
                }

                System.out.println("");
            }
        }
    }
}