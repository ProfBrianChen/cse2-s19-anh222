//Andrew Ha CSE 002 hw08 4/9/19
//The purpose of this program is to create an array that has a random number of lowercase and uppercase letters and be able to separate the array into 2 arrays using methods

import java.util.Random;
import java.util.Arrays;

public class Letters{

    public static String[] getAtoM(String[] stringArray){
        String[] AtoM = new String[]{"A","B","C","D","E","F","G","H","I","J","K","L","M","a","b","c","d","e","f","g","h","i","j","k","l","m"};             //string of both uppercase and lowercase letters from A to M
        String[] separatedArray;
        int newArrayLength = 0;
        int indexCounter = 0;

        for (int i = 0; i < stringArray.length; i++){           //checking how large the size of the separated arrays have to be
            for(int j = 0; j < AtoM.length; j++){
                if (stringArray[i] == AtoM[j]){
                    newArrayLength++;
                }
            }
        }
        separatedArray = new String[newArrayLength];

            for (int i = 0; i < stringArray.length; i++) {
                for (int j = 0; j < AtoM.length; j++) {
                    if (stringArray[i] == AtoM[j]) {                    //checking if each character in random character arrays matches any of the characters from AtoM
                        if (indexCounter > separatedArray.length - 1){
                            break;
                        }
                        else{
                            separatedArray[indexCounter] = stringArray[i];
                            indexCounter++;
                        }
                    }
                }
            }
        

        return separatedArray;
    }

    public static String[] getNtoZ(String[] stringArray){
        String[] NtoZ = new String[]{"N","O","P","Q","R","S","T","U","V","W","X","Y","Z","n","o","p","q","r","s","t","u","v","w","x","y","z"}; // string of both uppercase and lowercase letters from A to M
        String[] separatedArray;
        int newArrayLength = 0;
        int indexCounter = 0;

        for (int i = 0; i < stringArray.length; i++) {
            for (int j = 0; j < NtoZ.length; j++) {
                if (stringArray[i] == NtoZ[j]) {
                    newArrayLength++;
                }
            }
        }
        separatedArray = new String[newArrayLength];

        for (int i = 0; i < stringArray.length; i++) {
            for (int j = 0; j < NtoZ.length; j++) {
                if (stringArray[i] == NtoZ[j]) {                            //checking if each character in random character arrays matches any of the characters from NtoZ
                    if (indexCounter > separatedArray.length - 1) {
                        break;
                    } else {
                        separatedArray[indexCounter] = stringArray[i];
                        indexCounter++;
                    }
                }
            }
        }


        return separatedArray;
    }
    public static void main(String args[]){
        Random random = new Random();
        
        String[] alphabet = new String[]{"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};

        String[] randomLetters = new String[random.nextInt(11)];        //initializing and declaring array for random character array with a random size ranging between 0 and 10 (11 is exclusive)

        for(int i = 0; i < randomLetters.length; i++){
            int randomNumber = random.nextInt(alphabet.length);    //chooses random index from array of strings of alphabet
            randomLetters[i] = alphabet[randomNumber];
        }

        System.out.print("Random character array: ");       //printing character arrays
        for(int i = 0; i < randomLetters.length; i++){
            System.out.print(randomLetters[i]);
        }
        System.out.println("");

        System.out.print("AtoM characters: ");
        for(int i = 0; i < getAtoM(randomLetters).length; i++){
            System.out.print(getAtoM(randomLetters)[i]);
        }
        System.out.println("");

        System.out.print("NtoZ characters: ");
        for(int i = 0; i < getNtoZ(randomLetters).length; i++){
            System.out.print(getNtoZ(randomLetters)[i]);
        }
        System.out.println("");

    }
}