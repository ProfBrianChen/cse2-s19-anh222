//Andrew Ha CSE 002 hw08 4/9/19
//The purpose of this program is to ask the user for 5 integer the between 0 and 59 and compare them to 5 randomly generated integers. If the arrays completely match, the user wins, otherwise, the user loses.

import java.lang.Math;
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;

public class PlayLottery{

    public static boolean userWins(int[] user, int[] winning){
        int numbersMatch = 0;       //counter to check if all numbers match

        for(int i = 0; i < 5; i++){     //checking equality with each index
            if (user[i] == winning[i]){
                numbersMatch++;
            }
        }

        if(numbersMatch == 5){      //if all numbers match in both arrays, returns true
            return true;
        }
        else{
            return false;
        }
    }

    public static int[] numbersPicked(){
        Random random = new Random();
        int[] integers = new int[5];
        int numAdded = 0;               //counter for check how many numbers were added to the array
        int numbersMatch = 0;

        while(numAdded <= 4){
            int randInt = random.nextInt(60);

            for(int i = 0; i < integers.length; i++){       //generating random int and checking if the random int is a duplicate that is already in the array
                if (randInt == integers[i]){
                    numbersMatch++;
                    break;
                }
            }
            if(numAdded > 4){       //if there are enough numbers added to the array, the loop stops and returns the array *** if u put if statement here, the number is still added even though it is equal to one of indexes
                break;
            }
            else if (numbersMatch > 0){         //if there are matching numbers, loop will skip to next iteration to generate another random number
                numbersMatch = 0;
                continue;
            }
            else {
                integers[numAdded] = randInt;        //if random number does not match then it gets added to array
                numAdded++;
            }
        }
        return integers;
    }

    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        int[] user = new int[5];
        int numbersInput = 0;
        int numbersMatch = 0;

        System.out.println("Enter 5 numbers between 0 and 59: ");

        while(numbersInput <= 4){                       //loop only runs until the input array is filled
            boolean isInt = scan.hasNextInt();

            if (isInt){                             //checks if input is integer
                int number = scan.nextInt();

                if(number >= 0 && number <= 59){                //checking if integer is between 0 and 59
                    for (int i = 0; i < user.length; i++) {
                        if (number == user[i]) {                        //checking if integer input is same as other integers in array
                            System.out.println("Can't have duplicate numbers");
                            numbersMatch++;
                        }
                    }
                }
                else{
                    System.out.println("Invalid input");
                    continue;
                }


                if(numbersMatch > 0){
                    numbersMatch = 0;
                    continue;
                }
                else if (numbersInput > 4){
                    break;
                }
                else{
                    user[numbersInput] = number;
                    numbersInput++;
                    
                }
            }
            else {
                String junkword = scan.next();
                System.out.println("Invalid input");
            }
        }
        
        int [] randomNumbers = numbersPicked();             //generating array of random numbers

        System.out.print("The winning numbers are: ");

        System.out.print(randomNumbers[0]);
        for (int i = 1; i < randomNumbers.length; i++){
            System.out.print(", " + randomNumbers[i]);
        }

        System.out.println("");

        if(userWins(user, randomNumbers)){      //checking if user input matches random numbers
            System.out.println("You win");
        }
        else {
            System.out.println("You lose");
        }


    }
}