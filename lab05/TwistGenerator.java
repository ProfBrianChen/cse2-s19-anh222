
//Andrew Ha CSE 002 lab05
//The purpose of this program is to use for loops to print out a "twist" on the screen

import java.util.Scanner;

//Start of class
public class TwistGenerator {
    //Start of main method
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);  //initializing scanner

        //while loop to check to if input entered is a positive integer
        while (true) {
            System.out.println("Positive integer length: ");
            boolean isInteger = scan.hasNextInt();  //checks if input entered is integer
            int length = scan.nextInt();
            int lengthDivisibleByThree = length % 3;    //use modulus to check if length of twist is divisible by 3
            int i = 0;  //used as counter

            if (isInteger && length > 0) {
                if (lengthDivisibleByThree == 0) {
                    while (i < length - lengthDivisibleByThree) {
                        System.out.print(" /\\");
                        i += 3;
                    }
                    System.out.println("");
                    i = 0;
                    while (i < length - lengthDivisibleByThree) {
                        System.out.print("X  ");
                        i += 3;
                    }
                    System.out.println("");
                    i = 0;
                    while (i < length - lengthDivisibleByThree) {
                        System.out.print(" \\/");
                        i += 3;
                    }
                    System.out.println("");
                    i = 0;
                } else {
                    if (lengthDivisibleByThree == 1) {
                        System.out.print("\\");
                        while (i < length - lengthDivisibleByThree) {
                            System.out.print(" /\\");
                            i += 3;
                        }
                        System.out.println("");
                        i = 0;

                        System.out.print(" ");

                        while (i < length - lengthDivisibleByThree) {
                            System.out.print("X  ");
                            i += 3;
                        }
                        System.out.println("");
                        i = 0;

                        System.out.print("/");

                        while (i < length - lengthDivisibleByThree) {
                            System.out.print(" \\/");
                            i += 3;
                        }
                        System.out.println("");
                        i = 0;
                    } else if (lengthDivisibleByThree == 2) {
                        System.out.print("/\\");

                        while (i < length - lengthDivisibleByThree) {
                            System.out.print(" /\\");
                            i += 3;
                        }
                        System.out.println("");
                        i = 0;

                        System.out.print("  ");

                        while (i < length - lengthDivisibleByThree) {
                            System.out.print("X  ");
                            i += 3;
                        }
                        System.out.println("");
                        i = 0;

                        System.out.print("\\/");

                        while (i < length - lengthDivisibleByThree) {
                            System.out.print(" \\/");
                            i += 3;
                        }
                        System.out.println("");
                        i = 0;
                    }
                }
                break;
            } else {
                System.out.println("Invalid input");    //if user does not type in positive integer, the loops asks user again for positive integer
            }
        }

    }// end of main method
}//end of class