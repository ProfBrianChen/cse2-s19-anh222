//Andrew Ha CSE 002 hw07 3/26/19
//The purpose of this program is to analyze the characters of a string and see if they are all letters or not

import java.util.Scanner;       //importing required classes
import java.lang.Character;

//start of class
public class StringAnalysis {
    public static boolean allLetters(String string){            //method for checking if all characters in string are letters or not
        int nonLetters = 0;
        boolean isLetter;

        for (int i = 0; i <= string.length() - 1; i++){
            isLetter = Character.isLetter(string.charAt(i));
            if (!isLetter) {
                nonLetters++;
            }
        }
        if (nonLetters > 0){        //if there are characters that are not letters, the method returns false
            return false;
        }
        else {
            return true;
        }
    }

    public static boolean allLetters(String string, int i){     //method checking if specified number of characters in string are letters or not
        int nonLetters = 0;
        boolean isLetter;

        for (int j = 0; j <= i - 1; j++) {
            isLetter = Character.isLetter(string.charAt(j));
            if (!isLetter) {
                nonLetters++;
            }
        }
        if (nonLetters > 0) {
            return false;
        } else {
            return true;
        }
    }

    //start of main method
    public static void main (String args[]){                    
        Scanner scan = new Scanner(System.in);      //initialize new scanner

        while (true){
            System.out.println("Enter string you want to analyze");         //ask for string user wants to analyze
            boolean isInt = scan.hasNextInt();
            boolean isDouble = scan.hasNextDouble();

            if (!isInt && !isDouble){                                       //checking if input is a string or not and then either asking for valid input or analyze string
                String string = scan.nextLine();
                
                System.out.println("Would you like to analyze a certain number of characters or all characters? : Enter a number or type \"all\" ");
    
                while (true) {                                      //asking if user wants to analyze certain number of characters or all characters of string
                    isInt = scan.hasNextInt();
                    boolean isString = scan.hasNext();
                
                    if (isInt) {                                //if chosen number of characters is <= length of stirng, method that accepts input will be used
                        int number = scan.nextInt();
                        if (number <= string.length()){
                            System.out.println("Are the characters letters?: " + allLetters(string, number));
                            break;
                        }
                        else {
                            System.out.println("Are the characters letters?: " + allLetters(string));
                            break;
                        }
                        
                    } else if (isString) {
                        String wordAll = scan.next();
                        if (wordAll.equals("all")){
                            System.out.println("Are all characters letters?: " + allLetters(string));
                            break;
                        }
                        else {
                            System.out.println("Invalid input");
                        }
                    }
                    else {
                        System.out.println("Invalid input");
                    }
                }
                break;
            }
            else {
                System.out.println("Input should be string");
                String junkword = scan.nextLine();
            }
        }
    }//end of main method
}//end of class