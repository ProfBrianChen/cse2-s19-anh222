//Andrew Ha CSE 002 hw07 3/26/19
//The purpose of this program is to ask the user for an input of a specific shape the user wants to find the area of
//Then the program will ask the user to input dimensions

import java.util.Scanner;

//start of class
public class Area {
    public static double areaRectangle(double width, double length) {           //methods calculate the area based on shape and given dimensions
        return width*length;
    }

    public static double areaTriangle(double base, double height){
        return 0.5*base*height;
    }

    public static double areaCircle(double radius){
        return 3.14*radius*radius;
    }

    public static double checkDouble(){                         //checking if input in double or not
        double dimension;
        Scanner scan = new Scanner(System.in);

        while(true){
            boolean isDouble = scan.hasNextDouble();
            boolean isInt = scan.hasNextInt();

            if (isDouble && !isInt){
                dimension = scan.nextDouble();
                break;
            }
            else {
                System.out.println("Double required");
                String junkword = scan.next();
            }
        }
        return dimension;
    }
    //start of main method
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        
        String triangle = "traingle";
        String rectangle = "rectangle";
        String circle = "circle";

        System.out.println("Chosoe a shape you want to find the area of: rectangle, triangle, or circle");      //asks user which shape they want to find area of

        while (true){
            String shape = scan.next();

            if (shape.equals(rectangle)) {                      //different input for dimensions asked based on which shape the user wants area of
                System.out.println("Enter length: ");                   
                double rectangleLength = checkDouble();
                System.out.println("Enter wisth: ");
                double rectangleWidth = checkDouble();

                System.out.println("The area is: " + rectangleLength * rectangleWidth);
                break;

            } else if (shape.equals(triangle)) {
                System.out.println("Enter length of base: ");
                double triangleBase = checkDouble();
                System.out.println("Enter heigth: ");
                double traingleHeight = checkDouble();

                System.out.println("The area is: " + (traingleHeight * triangleBase * 0.5));
                break;

            } else if (shape.equals(circle)) {
                System.out.println("Enter radius: ");
                double radius = checkDouble();

                System.out.println("The area is: " + (3.14 * radius * radius));
                break;

            } else {
                System.out.println("Invalid input: acceptable inputs are \"triangle\", \"rectangle\", or \"circle\"");          //if input was not any of these shapes, user is asked again
            }
        } 
    } //end of main method
} //end of class
