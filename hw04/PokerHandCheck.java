// Andrew Ha CSE 2 HW04 2/19/19
// The purpose of this program is draw 5 random cards and identify whether the cards drawn are two pairs, pairs, three of a kind, or high card hand

import java.lang.Math;

//start of class
public class PokerHandCheck {
    //start of main method
    public static void main(String args[]) {
        int randomNumber = (int) (Math.random() * (51)) + 1; //initializing variable for random number

        // Initializing variables for values of each card drawn
        int cardValue1 = 0;
        int cardValue2 = 0;
        int cardValue3 = 0;
        int cardValue4 = 0;
        int cardValue5 = 0;

        System.out.println("You random cards were: \n");

        // Drawing 1st card
        if (randomNumber >= 1 && randomNumber <= 13) { // Different ranges between 1 and 52 to determine suit of the card
            switch (randomNumber) {
            case 1:
                System.out.println("the Ace of Diamonds");
                break;
            case 11:
                System.out.println("the Jack of Diamonds");
                break;
            case 12:
                System.out.println("the Queen of Diamonds");
                break;
            case 13:
                System.out.println("the King of Diamonds");
                break;
            default:
                System.out.println("the " + randomNumber + " of Diamonds");
                break;
            }
            cardValue1 = randomNumber;
        }
        if (randomNumber >= 14 && randomNumber <= 26) {
            switch (randomNumber % 13) { // Used modulus so that the random number chosen can be replaced by a value between 1 and 13 to determine the value of the card
            case 1:
                System.out.println("the Ace of Clubs");
                break;
            case 11:
                System.out.println("the Jack of Clubs");
                break;
            case 12:
                System.out.println("the Queen of Clubs");
                break;
            case 0:                                         // if random number is 26, the remainder would be 0
                System.out.println(" the King of Clubs");
                break;
            default:
                System.out.println("the " + (randomNumber % 13) + " of Clubs");
                break;
            }
            cardValue1 = randomNumber % 13;
        }
        if (randomNumber >= 27 && randomNumber <= 39) {
            switch (randomNumber % 26) {
            case 1:
                System.out.println("the Ace of Hearts");
                break;
            case 11:
                System.out.println("the Jack of Hearts");
                break;
            case 12:
                System.out.println("the Queen of Hearts");
                break;
            case 13:
                System.out.println("the King of Hearts");
                break;
            default:
                System.out.println("the " + (randomNumber % 26) + " of Hearts");
                break;
            }
            cardValue1 = randomNumber % 26;
        }
        if (randomNumber >= 40 && randomNumber <= 52) {
            switch (randomNumber % 39) {
            case 1:
                System.out.println("the Ace of Spades");
                break;
            case 11:
                System.out.println("the Jack of Spades");
                break;
            case 12:
                System.out.println("the Queen of Spades");
                break;
            case 13:
                System.out.println("the King of Spades");
                break;
            default:
                System.out.println("the " + (randomNumber % 39) + " of Spades");
                break;
            }
            cardValue1 = randomNumber % 39;
        }

        // Drawing 2nd card
        randomNumber = (int) (Math.random() * (51)) + 1; // Getting new random number to get new card from different shuffled deck
        if (randomNumber >= 1 && randomNumber <= 13) {
            switch (randomNumber) {
            case 1:
                System.out.println("the Ace of Diamonds");
            case 11:
                System.out.println("the Jack of Diamonds");
                break;
            case 12:
                System.out.println("the Queen of Diamonds");
                break;
            case 13:
                System.out.println("the King of Diamonds");
                break;
            default:
                System.out.println("the " + randomNumber + " of Diamonds");
                break;
            }
            cardValue2 = randomNumber;
        }
        if (randomNumber >= 14 && randomNumber <= 26) {
            switch (randomNumber % 13) {
            case 1:
                System.out.println("the Ace of Clubs");
            case 11:
                System.out.println("the Jack of Clubs");
                break;
            case 12:
                System.out.println("the Queen of Clubs");
                break;
            case 0:
                System.out.println("the King of Clubs");
                break;
            default:
                System.out.println("the " + (randomNumber % 13) + " of Clubs");
            }
            cardValue2 = randomNumber % 13;
        }
        if (randomNumber >= 27 && randomNumber <= 39) {
            switch (randomNumber % 26) {
            case 1:
                System.out.println("the Ace of Hearts");
                break;
            case 11:
                System.out.println("the Jack of Hearts");
                break;
            case 12:
                System.out.println("the Queen of Hearts");
                break;
            case 13:
                System.out.println("the King of Hearts");
                break;
            default:
                System.out.println("the " + (randomNumber % 26) + " of Hearts");
            }
            cardValue2 = randomNumber % 26;
        }
        if (randomNumber >= 40 && randomNumber <= 52) {
            switch (randomNumber % 39) {
            case 1:
                System.out.println("the Ace of Spades");
                break;
            case 11:
                System.out.println("the Jack of Spades");
                break;
            case 12:
                System.out.println("the Queen of Spades");
                break;
            case 13:
                System.out.println("the King of Spades");
                break;
            default:
                System.out.println("the " + (randomNumber % 39) + " of Spades");
            }
            cardValue2 = randomNumber % 39;
        }

        // Drawing 3rd card
        randomNumber = (int) (Math.random() * (51)) + 1;
        if (randomNumber >= 1 && randomNumber <= 13) {
            switch (randomNumber) {
            case 1:
                System.out.println("the Ace of Diamonds");
                break;
            case 11:
                System.out.println("the Jack of Diamonds");
                break;
            case 12:
                System.out.println("the Queen of Diamonds");
                break;
            case 13:
                System.out.println("the King of Diamonds");
                break;

            default:
                System.out.println("the " + randomNumber + " of Diamonds");
                break;
            }
            cardValue3 = randomNumber;
        }
        if (randomNumber >= 14 && randomNumber <= 26) {
            switch (randomNumber % 13) {
            case 1:
                System.out.println("the Ace of Clubs");
                break;
            case 11:
                System.out.println("the Jack of Clubs");
                break;
            case 12:
                System.out.println("the Queen of Clubs");
                break;
            case 0:
                System.out.println("the King of Clubs");
                break;

            default:
                System.out.println("the " + (randomNumber % 13) + " of Clubs");
            }
            cardValue3 = randomNumber % 13;
        }
        if (randomNumber >= 27 && randomNumber <= 39) {
            switch (randomNumber % 26) {
            case 1:
                System.out.println("the Ace of Hearts");
                break;
            case 11:
                System.out.println("the Jack of Hearts");
                break;
            case 12:
                System.out.println("the Queen of Hearts");
                break;
            case 13:
                System.out.println("the King of Hearts");
                break;

            default:
                System.out.println("the " + (randomNumber % 26) + " of Hearts");
            }
            cardValue3 = randomNumber % 26;
        }
        if (randomNumber >= 40 && randomNumber <= 52) {
            switch (randomNumber % 39) {
            case 1:
                System.out.println("the Ace of Spades");
                break;
            case 11:
                System.out.println("the Jack of Spades");
                break;
            case 12:
                System.out.println("the Queen of Spades");
                break;
            case 13:
                System.out.println("the King of Spades");
                break;

            default:
                System.out.println("the " + (randomNumber % 39) + " of Spades");
            }
            cardValue3 = randomNumber % 39;
        }

        // Drawing 4th card
        randomNumber = (int) (Math.random() * (51)) + 1;
        if (randomNumber >= 1 && randomNumber <= 13) {
            switch (randomNumber) {
            case 1:
                System.out.println("the Ace of Diamonds");
                break;
            case 11:
                System.out.println("the Jack of Diamonds");
                break;
            case 12:
                System.out.println("the Queen of Diamonds");
                break;
            case 13:
                System.out.println("the King of Diamonds");
                break;

            default:
                System.out.println("the " + randomNumber + " of Diamonds");
                break;
            }
            cardValue4 = randomNumber;
        }
        if (randomNumber >= 14 && randomNumber <= 26) {
            switch (randomNumber % 13) {
            case 1:
                System.out.println("the Ace of Clubs");
                break;
            case 11:
                System.out.println("the Jack of Clubs");
                break;
            case 12:
                System.out.println("the Queen of Clubs");
                break;
            case 0:
                System.out.println("the King of Clubs");
                break;

            default:
                System.out.println("the " + (randomNumber % 13) + " of Clubs");
            }
            cardValue4 = randomNumber % 13;
        }
        if (randomNumber >= 27 && randomNumber <= 39) {
            switch (randomNumber % 26) {
            case 1:
                System.out.println("the Ace of Hearts");
                break;
            case 11:
                System.out.println("the Jack of Hearts");
                break;
            case 12:
                System.out.println("the Queen of Hearts");
                break;
            case 13:
                System.out.println("the King of Hearts");
                break;

            default:
                System.out.println("the " + (randomNumber % 26) + " of Hearts");
            }
            cardValue4 = randomNumber % 26;
        }
        if (randomNumber >= 40 && randomNumber <= 52) {
            switch (randomNumber % 39) {
            case 1:
                System.out.println("the Ace of Spades");
                break;
            case 11:
                System.out.println("the Jack of Spades");
                break;
            case 12:
                System.out.println("the Queen of Spades");
                break;
            case 13:
                System.out.println("the King of Spades");
                break;

            default:
                System.out.println("the " + (randomNumber % 39) + " of Spades");
            }
            cardValue4 = randomNumber % 39;
        }

        // Drawing 5th card
        randomNumber = (int) (Math.random() * (51)) + 1;
        if (randomNumber >= 1 && randomNumber <= 13) {
            switch (randomNumber) {
            case 1:
                System.out.println("the Ace of Diamonds");
                break;
            case 11:
                System.out.println("the Jack of Diamonds");
                break;
            case 12:
                System.out.println("the Queen of Diamonds");
                break;
            case 13:
                System.out.println("the King of Diamonds");
                break;

            default:
                System.out.println("the " + randomNumber + " of Diamonds");
                break;
            }
            cardValue5 = randomNumber;
        }
        if (randomNumber >= 14 && randomNumber <= 26) {
            switch (randomNumber % 13) {
            case 1:
                System.out.println("the Ace of Clubs");
                break;
            case 11:
                System.out.println("the Jack of Clubs");
                break;
            case 12:
                System.out.println("the Queen of Clubs");
                break;
            case 0:
                System.out.println("the King of Clubs");
                break;

            default:
                System.out.println("the " + (randomNumber % 13) + " of Clubs");
            }
            cardValue5 = randomNumber % 13;
        }
        if (randomNumber >= 27 && randomNumber <= 39) {
            switch (randomNumber % 26) {
            case 1:
                System.out.println("the Ace of Hearts");
                break;
            case 11:
                System.out.println("the Jack of Hearts");
                break;
            case 12:
                System.out.println("the Queen of Hearts");
                break;
            case 13:
                System.out.println("the King of Hearts");
                break;

            default:
                System.out.println("the " + (randomNumber % 26) + " of Hearts");
            }
            cardValue5 = randomNumber % 26;

        }
        if (randomNumber >= 40 && randomNumber <= 52) {
            switch (randomNumber % 39) {
            case 1:
                System.out.println("the Ace of Spades");
                break;
            case 11:
                System.out.println("the Jack of Spades");
                break;
            case 12:
                System.out.println("the Queen of Spades");
                break;
            case 13:
                System.out.println("the King of Spades");
                break;

            default:
                System.out.println("the " + (randomNumber % 39) + " of Spades");
            }
            cardValue5 = randomNumber % 39;
        }

        //initializing boolean variables to check for special hands
        boolean twoPair = ((cardValue1 == cardValue2) && (cardValue3 == cardValue4)) // Checking for 2 pairs
                || ((cardValue1 == cardValue2) && (cardValue3 == cardValue5))
                || ((cardValue1 == cardValue2) && (cardValue4 == cardValue5))
                || ((cardValue1 == cardValue3) && (cardValue4 == cardValue5))
                || ((cardValue1 == cardValue3) && (cardValue2 == cardValue4))
                || ((cardValue1 == cardValue3) && (cardValue2 == cardValue5))
                || ((cardValue1 == cardValue4) && (cardValue2 == cardValue5))
                || ((cardValue1 == cardValue4) && (cardValue3 == cardValue5))
                || ((cardValue1 == cardValue4) && (cardValue2 == cardValue3))
                || ((cardValue1 == cardValue5) && (cardValue2 == cardValue3))
                || ((cardValue1 == cardValue5) && (cardValue2 == cardValue4))
                || ((cardValue1 == cardValue5) && (cardValue3 == cardValue5))
                || ((cardValue2 == cardValue3) && (cardValue4 == cardValue5))
                || ((cardValue2 == cardValue3) && (cardValue1 == cardValue4))
                || ((cardValue2 == cardValue3) && (cardValue1 == cardValue5))
                || ((cardValue2 == cardValue4) && (cardValue3 == cardValue5))
                || ((cardValue3 == cardValue4) && (cardValue1 == cardValue5))
                || ((cardValue3 == cardValue4) && (cardValue2 == cardValue5));

        boolean threeOfAKind = ((cardValue1 == cardValue2) && (cardValue1 == cardValue3)) // Checking for three of a kind
                || ((cardValue1 == cardValue3) && (cardValue1 == cardValue4))
                || ((cardValue1 == cardValue4) && (cardValue1 == cardValue5))
                || ((cardValue1 == cardValue2) && (cardValue1 == cardValue4))
                || ((cardValue1 == cardValue2) && (cardValue1 == cardValue5))
                || ((cardValue1 == cardValue3) && (cardValue1 == cardValue5))
                || ((cardValue2 == cardValue3) && (cardValue2 == cardValue4))
                || ((cardValue2 == cardValue4) && (cardValue2 == cardValue5))
                || ((cardValue2 == cardValue3) && (cardValue2 == cardValue5))
                || ((cardValue3 == cardValue4) && (cardValue3 == cardValue5));

        boolean pair = (cardValue1 == cardValue2) || (cardValue1 == cardValue3) || (cardValue1 == cardValue4) // Checking for pairs
                || (cardValue1 == cardValue5) || (cardValue2 == cardValue3) || (cardValue2 == cardValue4)
                || (cardValue2 == cardValue5) || (cardValue3 == cardValue4) || (cardValue3 == cardValue5)
                || (cardValue4 == cardValue5);

        System.out.println(""); //print new line
      
        if (twoPair){                                       // Checking for which type of special hand the cards drawn are
            System.out.println("You have a two pair!");
        } else if (threeOfAKind){
            System.out.println("You have a three of kind!");
        } else if (pair){
            System.out.println("you have a pair!");
        } else {
            System.out.println("You have a high card hand!");
        }
    }//end of main method
}// end of class