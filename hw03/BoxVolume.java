//Andrew Ha CSE 002 HW03 2/8/19
//The purpose of this program is to prompt the user for inputs on the dimensions of a box and then calculate and print the volume of the box

import java.util.Scanner;

//start of class
public class BoxVolume{
  //start of main method
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in); //initalize new scanner
    
    System.out.print("The width side of the box is: "); //asking user to input width
    double width = myScanner.nextDouble(); //scanner used to input width
    
    System.out.print("The length of the box is: "); //ask user to input length
    double length = myScanner.nextDouble(); //scanner used to input length
    
    System.out.print("The height of the box is: "); //ask user to input height 
    double height = myScanner.nextDouble(); //scanner used to input height
    
    double volume = width*length*height; //calculating the volume
    
    System.out.println("The volume inside the box is: " + (int)volume); //printing volume as int because sample run shown on hw prints volume as int
  } //end of main method
} //end of class

