// Andrew Ha CSE 002 2/8/19 HW03
//The purpose of this program is to the receive user input and perform arithmetic operations with the inputs

import java.util.Scanner;

//start of class
public class Convert{
  //start of main method
      public static void main(String args[]){
        Scanner myScanner = new Scanner(System.in); //initialize scanner to read input
        double inchesPerMeter = 39.3701; //conversion factor used convert meters to inches
        System.out.print("Enter the distance in meters: ");
        double distance = myScanner.nextDouble(); //ask user to input distance
        double distanceToInches = distance * inchesPerMeter; //convert input distance to inches
        System.out.println(distance + " meters is " + (int)(distanceToInches*10000)/10000.0 + " inches.");//printing conversion and formatting value to only print upto 4 decimal places
    
  } //end of main method
}// end of class