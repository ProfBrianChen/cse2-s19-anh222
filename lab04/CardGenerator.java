// Andrew Ha CSE 002 Lab04 2/15/19

import java.lang.Math; //importing for Math method

// start of method
public class CardGenerator{
  // start of main class
  public static void main(String args[]){
    int randomNumber = (int)((Math.random() * (51)) + 1); //getting random number between 1 and 52
    
    int randomNumberClubs = randomNumber % 13; //modulus allows value of randomNumber to fall in range of 1-13 so the randomNumber can be used for print statements and determining value of card
    int randomNumberHearts = randomNumber % 26;
    int randomNumberSpades = randomNumber % 39;
    
    if (randomNumber >= 1 && randomNumber <= 13){   //Choosing random card for Diamonds and printing value and class
         switch (randomNumber){
           case 11:
             System.out.println("You picked the Jack of Diamonds");
             break;
           case 12:
             System.out.println("You picked the Queen of Diamonds");  
             break;
           case 13:
             System.out.println("You picked the King of Diamonds");
             break;
           case 14:
             System.out.println("You picked the Ace of Diamonds");
             break;
           default:
             System.out.println("You picked the " + randomNumber + " of Diamonds");
             break;
         }
       }
      if (randomNumber >= 14 && randomNumber <= 26){    //Choosing random card for Clubs and printing value and class
         switch (randomNumberClubs){
           case 11:
             System.out.println("You picked the Jack of Clubs");
             break;
           case 12:
             System.out.println("You picked the Queen of Clubs");  
             break;
           case 13:
             System.out.println("You picked the King of Clubs");
             break;
           case 14:
             System.out.println("You picked the Ace of Clubs");
             break;
           default:
             System.out.println("You picked the " + randomNumberClubs + " of Clubs");
         }
      }
    if (randomNumber >= 27 && randomNumber <= 39){    //Choosing random card for Hearts and printing value and class
         switch (randomNumberHearts){
           case 11:
             System.out.println("You picked the Jack of Hearts");
             break;
           case 12:
             System.out.println("You picked the Queen of Hearts");  
             break;
           case 13:
             System.out.println("You picked the King of Hearts");
             break;
           case 14:
             System.out.println("You picked the Ace of Hearts");
             break;
           default:
             System.out.println("You picked the " + randomNumberHearts + " of Hearts");
         }
    }
    if (randomNumber >= 40 && randomNumber <= 52){    //Choosing random card for Spades and printing value and class
         switch (randomNumberSpades){
           case 11:
             System.out.println("You picked the Jack of Spades");
             break;
           case 12:
             System.out.println("You picked the Queen of Spades");  
             break;
           case 13:
             System.out.println("You picked the King of Spades");
             break;
           case 14:
             System.out.println("You picked the Ace of Spades");
             break;
           default:
             System.out.println("You picked the " + randomNumberSpades + " of Spades");
     }
   }
  } // end of method
} // end of main class