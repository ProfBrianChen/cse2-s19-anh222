//Andrew Ha CSE 002 hw10
//The purpose of this program is create a 2D array that represents city blocks with different populations that are randomly generated. 
//Robots are then placed on random block and are moved to toward the east until they disappear when they have reached the most eastern block.


import java.util.Random;
import java.lang.Math;

//start of main class
public class RobotCity{

    public static int[][] buildCity(){                      //method for randomly generating 2D array for city
        Random rand = new Random();

        int eastWest = rand.nextInt(6) + 10;
        int northSouth = rand.nextInt(6) + 10;

        int[][] city = new int[eastWest][northSouth];

        for(int i = 0; i < city.length; i++){
            for(int j = 0; j < city[i].length; j++){
                city[i][j] = (int)((Math.random() * (999 - 100)) + 100);
            }
        }

        return city;
    }

    public static void display(int[][] city){               //printing out array with each block taking up the same space when printed
        for (int j = city[0].length - 1; j >= 0; j--){
            System.out.print("[");
            for (int i = 0; i < city.length; i++){
                System.out.printf("%4d", city[i][j]);       //each integer should take up 4 spaces due to possibility of integer being negative (negative sign takes up 1 more space)
                System.out.print(" ");
            }
            System.out.println("]");
        }
        System.out.println("");
    }

    public static void invade(int[][] city, int k){                 //randomly placing robots on the city blocks
        Random rand = new Random();

        for(int i = 0; i < k; i++){
        int northSouth = rand.nextInt(city[0].length);
        int eastWest = rand.nextInt(city.length);
            if (city[eastWest][northSouth] > 0){                                //checking if there is already a robot in city block before adding one
                city[eastWest][northSouth] = -1 * city[eastWest][northSouth];
            }
        }
    }

    public static void update(int[][] city){                //updating robots by shifting robots to east
        for (int j = 0; j < city[0].length; j++){
            for (int i = city.length - 1; i >= 0; i--){
                if(city[i][j] < 0){
                    if(i == city.length - 1){
                        city[i][j] = -1 * city[i][j];
                    }
                    else{
                        city[i][j] = -1 * city[i][j];
                        city[i+1][j] *= -1;
                        i++;
                    }
                }
            }
        }
    }

    //start of main method
    public static void main(String args[]){
        Random rand = new Random();
        int[][] city = buildCity();

        display(city);
        invade(city, rand.nextInt(10) + 1);
        display(city);

        for(int i = 0; i < 5; i++){
            update(city);
            display(city);
        }
    }// end of main method
}// end of main class