//Andrew Ha CSE 002 hw10
//The purpose of this program is to create a shuffled deck of 52 cards and draw the first 5 cards. 
//Then check whether the draw is a straight or not. 1,000,000 draws will be checked to see the percentage of straights. This value is compared to the probability according to Wikipedia

import java.util.Random;
import java.lang.Math;

//start of class
public class Straight{

    public static int[] generateShuffledDeck(){     //generating shuffled deck of 52 cards
        Random rand = new Random();

        int[] deck = new int[52];

        for(int j = 0; j < deck.length; j++){       //recognizing different suites of deck
            if(j >= 0 && j <= 12){
                deck[j] = j;
            }
            else if(j >= 13 && j <= 25){
                deck[j] = j % 13;
            }
            else if (j >= 26 && j <= 38) {
                deck[j] = j % 26;
            }
            else if (j >= 39 && j <= 51) {
                deck[j] = j % 39;
            }
        }

        for(int i = 0; i < deck.length; i++){
            int index = rand.nextInt(52);

            int temp = deck[index];
            deck[index] = deck[i];
            deck[i] = temp;

        }

        return deck;
    }

    public static int[] draw(int[] deck){               //drawing 1st 5 cards from array
        int[] drawDeck = new int[5];

        for(int i = 0; i < drawDeck.length; i++){
            drawDeck[i] = deck[i];
        }

        return drawDeck;
    }

    public static int straight(int[] drawDeck, int k){  //using selection sort to return value of kth lowest card
        int minimumIndex = 0;
        
        if (k < 0 || k > 5){
            return -1;                  //returns -1 if k is not between 0 and 5
        }
        else{
            for (int i = 0; i < drawDeck.length; i++){      
                int minimum = 999;
                for(int j = i; j < drawDeck.length; j++){
                    if (drawDeck[j] < minimum){
                        minimum = drawDeck[j];
                        minimumIndex = j; 
                    }
                }

                int temp = drawDeck[i];
                drawDeck[i] = drawDeck[minimumIndex];
                drawDeck[minimumIndex] = temp;
            }
        }

        return drawDeck[k - 1];
    }

    public static double generateRandomsTest(int howManyToGenerate){        //generating shuffled decks and checking for straights and returning percetnage of straights
        int straights = 0;
        double chanceOfDrawingStraight = 0;

        for(int i = 0; i < howManyToGenerate; i++){     //randomly generating given number of draws and testing if they are straights
            int[] deck = generateShuffledDeck();

            int[] draw = draw(deck);

            if( ((straight(draw, 5) - straight(draw, 4)) == 1) && 
                ((straight(draw, 4) - straight(draw, 3)) == 1) && 
                ((straight(draw, 3) - straight(draw, 2)) == 1) && 
                ((straight(draw, 2) - straight(draw, 1)) == 1) ){
                    straights++;
                }
        }

        chanceOfDrawingStraight = (straights / (double) howManyToGenerate) * 100;   //calculating percentage of straights
        return chanceOfDrawingStraight;
    }

    //start of main method
    public static void main(String args[]){

        double chance = generateRandomsTest(1000000);   //generating 1,000,000 draws and calculating percentage of straights
        
        System.out.print(chance);
        System.out.println("%");

            
    }//end of main method
}//end of class