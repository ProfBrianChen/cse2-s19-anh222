
//Andrew Ha CSE 002 lab06
//The purpose of this lab is to use loops print out pyramids of a given size based on an integer input

import java.util.Scanner; //importing scanner
//start of class
public class PatternB {
    // start of main method
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);

        while (true) {
            System.out.println("Integer between 1-10");
            boolean isInt = scan.hasNextInt();

            if (isInt) {
                int number = scan.nextInt();
                if (number > 1 && number < 10) {
                    for (int i = number; i > 0; i--) {
                        for (int j = 1; j <= i; j++) {
                            System.out.print(j + " ");
                        }
                        System.out.println("");
                    }
                    break;
                } else {
                    System.out.println("Invalid input");
                }
            } else {
                System.out.println("Invalid input");
                String junkword = scan.next();
            }
        }
    }// end of main method
}// end of class