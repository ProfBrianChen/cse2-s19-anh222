
//Andrew Ha CSE 002 lab06
//The purpose of this lab is to use loops print out pyramids of a given size based on an integer input

import java.util.Scanner; //importing scanner

//start of class
public class PatternC {
    // start of main method
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);

        while (true) {
            System.out.println("Integer between 1-10");
            boolean isInt = scan.hasNextInt();

            if (isInt) {
                int number = scan.nextInt();
                if (number > 1 && number < 10) {
                  for (int k = 1; k <= number; k++){
                    for (int i = k; i <= number - 1; i++) {
                        System.out.print(" ");
                    }
                    for (int j = k; j > 0; j--) {
                            System.out.print(j);
                    }
                        System.out.println("");
                  }
                    break;
                } else {
                    System.out.println("Invalid input");
                }
            } else {
                System.out.println("Invalid input");
                String junkword = scan.next();
            }
        }
    }// end of main method
}// end of class