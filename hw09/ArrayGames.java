//Andrew Ha CSE 002 hw09 4/16/19
//The purpose of this program is to ask the user for input on whether they want to shorten a randomly generated array or insert a randomly generated number into an array

import java.util.Random;
import java.util.Scanner;
import java.lang.Math;


public class ArrayGames{

    public static int[] generate(){
        int arraySize = (int)((Math.random() * (20 - 10 + 1)) + 10);            //generating random array size from 10-20   
        Random rand = new Random();

        int[] array = new int[arraySize];

        for(int i = 0; i <= array.length - 1; i++){                         //setting values of array to random numbers from 0-69 until the loop iterates through entire array as shown in assignment
            int randomNumber = rand.nextInt(70);
            array[i] = randomNumber;
        }

        return array;
    }

    public static void print(int[] array){                      //printing array
        System.out.print("{");
        System.out.print(array[0]);
        
        for(int i = 1; i < array.length; i++){
            System.out.print("," + array[i]);
        }

        System.out.println("}");
    }

    public static int[] insert(int[] array1, int[] array2){
        Random rand = new Random();
        
        int[] newArray = new int[array1.length + array2.length];    //generating new array which will include both 1st and 2nd array
        int randomIndex = rand.nextInt(10);                         //generating random index

        for (int i = 0; i < randomIndex; i++){      //adding members of 1st array before randomly chosen index to new array
            newArray[i] = array1[i];
        }

        for(int i = 0; i < array2.length; i++){     //adding members of 2nd array into first array at random index
            newArray[randomIndex + i] = array2[i]; 
        }

        for (int i = 0; i < newArray.length - array2.length - randomIndex; i++){     //adding the index elements that are after the added numbers from the 1st array
            newArray[i + array2.length + randomIndex] = array1[i + randomIndex];
        }

        return newArray;
    }

    public static int[] shorten(int[] array, int index){       //shortening input array by removing value at input index
        int[] newArray = new int[array.length - 1];

        if (index > array.length - 1){     //if input index is greater than how many indexes in the input array, then the input array itself is returned
            return array;
        }
        else{
            for(int i = 0; i < newArray.length; i++){
                if(i >= index){                         //if i > input index, newArray is assigned the values of the indexes i + 1 (skips index that matches input index) of the input array
                    newArray[i] = array[i + 1];
                }
                else{
                    newArray[i] = array[i];
                }
            }
        }
        return newArray;
    }

    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        String junkword = "";
        String type = "";
        
        System.out.println("Enter \"s\" for shorten or \"i\" for insert");
        while(true){
            boolean isInt = scan.hasNextInt();                 //making sure input is not anything other than a string
            boolean isDouble = scan.hasNextDouble();

            if (isInt || isDouble){
                System.out.println("Invalid input");
                junkword = scan.next();
            }
            else{
                type = scan.next();
                if (type.equals("s") || type.equals("i")){
                    break;
                }
                else{
                    System.out.println("Invalid input");
                }
            }
        }

        if (type.equals("s")){              //checking whether shorten() or insert() should be run based on user input
            int[] array = generate();
            int index = rand.nextInt(15);

            int[] newArray = shorten(array, index);

            System.out.print("Input 1: ");
            System.out.print("{" + array[0]);
            for(int i = 1; i < array.length; i++){
                System.out.print("," + array[i]);
            }
            System.out.print("}\t");
            System.out.println("Input 2: " + index);

            System.out.print("Output: ");
            System.out.print("{" + newArray[0]);
            for (int i = 1; i < newArray.length; i++) {
                System.out.print("," + newArray[i]);
            }
            System.out.println("}");
        }
        else if (type.equals("i")){
            int[] array1 = generate();
            int[] array2 = generate();

            int[] newArray = insert(array1, array2);

            System.out.print("Input 1: ");
            System.out.print("{" + array1[0]);
            for(int i = 1; i < array1.length; i++){
                System.out.print("," + array1[i]);
            }

            System.out.print("}\t");

            System.out.print("Input 2: ");
            System.out.print("{" + array2[0]);
            for(int i = 1; i < array2.length; i++){
                System.out.print("," + array2[i]);
            }
            System.out.print("}\t");

            System.out.println("");

            System.out.print("Output: ");
            System.out.print("{" + newArray[0]);
            for (int i = 1; i < newArray.length; i++) {
                System.out.print("," + newArray[i]);
            }
            System.out.println("}");
        }

    }
}