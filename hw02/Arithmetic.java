//Andrew Ha 2/5/19 CSE 002 hw02
//Purpose of this program is to use data stroed in variables to perform calculations,
//in the form of purchasing clothing, and printing the numerical output of the calculations.

//start of class
public class Arithmetic{
  //start of main method
  public static void main(String args[]){
    //Number of pairs of pants
    int numPants = 3;
	  //Cost per pair of pants
	  double pantsPrice = 34.98;

	  //Number of sweatshirts
	  int numShirts = 2;
	  //Cost per shirt
	  double shirtPrice = 24.99;

	  //Number of belts
	  int numBelts = 1;
	  //cost per belt
	  double beltCost = 33.99;

	  //the tax rate
	  double paSalesTax = 0.06;
	
    //total cost of all pants
	  double totalPants = (numPants*pantsPrice);
    //total cost of all shirts
    double totalShirts = (numShirts*shirtPrice);
    //total cost of all belts
    double totalBelts = (numBelts*beltCost);
    
    //total sales tax for pants
	  double salesTaxPants = paSalesTax*totalPants;
    //total sales tax for shirts
	  double salesTaxShirts = paSalesTax*totalShirts;
    //total sales tax for belts
	  double salesTaxBelts = paSalesTax*totalBelts;
    
    //total cost before tax
	  double totalCostBeforeTax = totalPants + totalShirts + totalBelts;
    //total sales tax
	  double totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelts;
    //total cost including sales tax
	  double totalTransaction = totalCostBeforeTax + totalSalesTax;
	
    //printing out calculations
    System.out.println("Total cost of pants: " + "$" + (int)(totalPants*100) / 100.0);
    System.out.println("Total cost of shirts: " + "$" + (int)(totalShirts*100) / 100.0);
    System.out.println("Total cost of belts: " + "$" + (int)(totalBelts*100) / 100.0);
    System.out.println("Total sales tax for pants: " + "$" + (int)(salesTaxPants*100) / 100.0);
    System.out.println("Total sales tax for shirts: " + "$" + (int)(salesTaxShirts*100) / 100.0);
    System.out.println("Total sales tax for belts: " + "$" + (int)(salesTaxBelts*100) / 100.0);
    System.out.println("Total cost before sales tax: " + "$" + (int)(totalCostBeforeTax*100) / 100.0);
    System.out.println("Total sales tax: " + "$" + (int)(totalSalesTax*100) / 100.0);
    System.out.println("Total cost including sales tax: " + "$" + (int)(totalTransaction*100) / 100.0);
  } //end of main method
} //end of class
	
  
