//Andrew Ha 2/1/19 CSE 002 Lab02
//The purpose of this program is to 


public class Cyclometer {

    // start of main method
    public static void main(String[] args) {
        // initiating variables
        int secsTrip1 = 480;  //the amount of time trip 1 took in seconds
        int secsTrip2 = 3220;  //the amount of time trip 2 took in seconds
        int countsTrip1 = 1561;  //the number of the rotations of the front wheel for trip 1
        int countsTrip2 = 9037; //the number of rotations of the front wheel for trip 2

        double wheelDiameter = 27.0, //diameter of the wheel
                PI = 3.14159, //value of pi
                feetPerMile = 5280, //conversion factor for feet per mile
                inchesPerFoot = 12, //conversion factor for inches per foot
                secondsPerMinute = 60;  //conversion factor for seconds for minute
        double distanceTrip1, distanceTrip2, totalDistance;  //initializing variables for distances of the trip 1, trip 2, and total distance of both trips
      
        // Printing Trip 1 and 2
        System.out.println("Trip 1 took "+ (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
        System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");

        // Calculating the distance of the trips
        distanceTrip1 = countsTrip1 * wheelDiameter * PI;
        // Above gives distance in inches
        //(for each count, a rotation of the wheel travels
        //the diameter in inches times PI)
        distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
        distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //Give distance in miles
        totalDistance = distanceTrip1 + distanceTrip2; //total distance of trips 1 and 2

        //Printing out output data
        System.out.println("Trip 1 was " + distanceTrip1 + " miles");
        System.out.println("Trip 2 was " + distanceTrip2 + " miles");
        System.out.println("The total distance was " + totalDistance + " miles");

    }  //end of main method   
} //end of class          

