//Andrew Ha CSE 002 hw05
//The purpose of this program is to use loops to check if the user inputs are of the correct type for certain information

import java.util.Scanner;       //import scanner tool

//start of class
public class Hw05 {
    //start of main method
    public static void main (String args[]){
        Scanner scan = new Scanner(System.in);      //initialize scanner

        while(true){
            System.out.println("Coursenumber: ");
            
            boolean isInt = scan.hasNextInt();  //checks if input is integer

            if (isInt){
                int courseNumber = scan.nextInt();  //if input type is correct, the integer is read and while loop breaks and moves on to next while loop
                break;                                  
            }
            else {
                System.out.println("Integer input required");
                String junkword = scan.next();       //if wrong input type, gives error message and repeats loop if input is
                                                     // not required type, next() removes the input
            }
        }

        while(true){
            System.out.println("Department name: ");

            String departmentName = scan.nextLine();
            boolean isString = scan.hasNextLine();          //checking if input is a string
            boolean isDouble = scan.hasNextDouble();
            boolean isInt = scan.hasNextInt();
            

            if (!isDouble && !isInt) {
                break;
            } else {
                System.out.println("String input required");
            }
        }

        while (true) {
            System.out.println("Meets per week: ");
            String readNextLine = scan.nextLine();
            boolean isInt = scan.hasNextInt();
            

            if (isInt) {
                int meetsPerWeek = scan.nextInt();
                break;
            } else {
                System.out.println("Integer input required");
            }
        }

        while (true) {
            System.out.println("Class start time: ");   //Assumed that class start time is a string. (Ex. "7:30 AM")

            String readNextLine = scan.nextLine();
            boolean isDouble = scan.hasNextDouble();
            boolean isInt = scan.hasNextInt();
            

            if (!(isDouble && isInt)) {
                break;
            } else {
                System.out.println("String input required");
            }
        }

        while (true) {
            System.out.println("Instructor name: ");

            String instructorName = scan.nextLine();
            boolean isDouble = scan.hasNextDouble();
            boolean isInt = scan.hasNextInt();

            if (!(isDouble && isInt)) {
                break;
            } else {
                System.out.println("String input required");
            }
        }
        
        while (true) {
            System.out.println("Number of students: ");

            String readNextLine = scan.nextLine();
            boolean isInt = scan.hasNextInt();
            

            if (isInt) {
                break;
            } else {
                System.out.println("Integer input required");
            }
        }
        
    }//end of main method
}//end of class