//Andrew Ha CSE 002 lab09
//The purpose of this lab is to ask the user for input for array size, integer search term, and whether they want to perform either linear or binary search


import java.util.Random;
import java.util.Scanner;

public class lab09{

    public static int[] generateArray(int size){
        Random rand = new Random();
        
        int array[] = new int[size];

        for(int i = 0; i < array.length; i++){
            array[i] = rand.nextInt(array.length);
        }

        return array;
    }

    public static int[] arrayRandomAscending(int size){
        Random rand = new Random();

        int array[] = new int[size];

        for (int i = 0; i < array.length; i++){
            if(i == 0){
                array[i] = rand.nextInt(6);
            }
            else {
                array[i] = array[i-1] + rand.nextInt(6);
            }
        }
        return array;
    }

    public static int linearSearch(int[] array, int search){
        int index = 0;

        for (int i = 0; i < array.length; i++){
            if (array[i] == search){
                index = i;
                break;
            }
            else {
                index = -1;
            }
        }
        return index;
    }

    public static int binarySearch(int[] array, int search){
        int first = 0;
        int last = array.length - 1;

        while (first <= last) {
            int mid = first + (last - first) / 2;

            // Check if integer search term is present at mid
            if (array[mid] == search)
                return mid;

            // If interger search term greater, ignore left half
            if (array[mid] < search)
                first = mid + 1;

            // If integer search term is smaller, ignore right half
            else
                last = mid - 1;
        }

        // if we reach here, then element was
        // not present
        return -1;
    }


    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        
        int arraySize = 0;
        int integerSearch = 0;
        String junkword = "";
        String searchType = "";

        System.out.println("Type \"L\" for linear or \"B\" for binary");
        
        while (true){                                       //checking input for choosing either linear or binary
            boolean isInt = scan.hasNextInt();
            boolean isDouble = scan.hasNextDouble();

            if (isInt || isDouble){
                junkword = scan.next();
                System.out.println("Invalid input");
                continue;
            }
            else{
                String word = scan.next();

                if(word.equals("L") || word.equals("B")){
                    searchType = word;
                    break;
                }
                else{
                    System.out.println("Invalid input");
                }
            }

        }

        System.out.println("Array size: ");

        while(true){                                //checking input for array size
            boolean isInt = scan.hasNextInt();
            boolean isDouble = scan.hasNextDouble();

            if (isInt){
                arraySize = scan.nextInt();
                if(arraySize < 0){
                    System.out.println("Invalid input");
                }
                else{
                    break; 
                }
            }
            else if(isDouble){
                System.out.println("Invalid input");
                junkword = scan.next();
            }
            else{
                System.out.println("Invalid input");
                junkword = scan.next();
            }
        }

        System.out.println("Interger search term: ");

        while (true) {                                  //checking input for integer search term
            boolean isInt = scan.hasNextInt();
            boolean isDouble = scan.hasNextDouble();

            if (isInt) {
                integerSearch = scan.nextInt();

                if (integerSearch < 0) {
                    System.out.println("Invalid input");
                } else {
                    break;
                }

            } else if (isDouble) {
                System.out.println("Invalid input");
                junkword = scan.next();
            } else {
                System.out.println("Invalid input");
                junkword = scan.next();
            }
        }


        int[] array;        

        if (searchType.equals("L")){                //choosing which array to print and whether to do binary or linear search
            int[] randArray = generateArray(arraySize);

            System.out.print("Array: ");
            
            for (int i : randArray){
                System.out.print(i + " ");
            }

            System.out.println("");

            System.out.print("Index location: ");
            System.out.println(linearSearch(randArray, integerSearch));
        }
        else if (searchType.equals("B")){
            int[] randAscending = arrayRandomAscending(arraySize);

            System.out.print("Array: ");

            for (int i : randAscending) {
                System.out.print(i + " ");
            }

            System.out.println("");

            System.out.print("Index location: ");
            System.out.println(linearSearch(randAscending, integerSearch));
        }
    }
}