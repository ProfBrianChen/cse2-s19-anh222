//Andrew Ha CSE 002 lab10

import java.util.Random;

public class lab10 {

    public static int[][] increasingMatrix(int width, int height, boolean format){      //format = true > row-major // false = column-major
        int increasingNumber = 1;
        
        if(format){
            int[][] rowMajor = new int[height][width];

            for(int i = 0; i < rowMajor.length; i++){
                for(int j = 0; j < rowMajor[0].length; j++){
                    rowMajor[i][j] = increasingNumber;
                    increasingNumber++; 
                }
            }

            return rowMajor;
        }
        else {
            int[][] columnMajor = new int[width][height];

            for (int i = 0; i < columnMajor[0].length; i++) {
                for (int j = 0; j < columnMajor.length; j++) {
                    columnMajor[j][i] = increasingNumber;
                    increasingNumber++;
                }
            }

            return columnMajor;
        }
    }

    public static void printMatrix(int[][] array, boolean format){
        if(format){
            for(int height = 0; height < array.length; height++){
                System.out.print("[");
                for(int width = 0; width < array[height].length; width++){
                    System.out.printf("%2d", array[height][width]);
                    System.out.print(" ");
                }
                System.out.println("]");
            }
        }
        else {
            for (int width = 0; width < array[0].length; width++) {
                System.out.print("[");
                for (int height = 0; height < array.length; height++) {
                    System.out.printf("%2d", array[height][width]);
                    System.out.print(" ");
                }
                System.out.println("]");
            }
        }
    }

    public static int[][] translate(int[][] array){
        int height = array[0].length;
        int width = array.length;
        
        int[][] rowMajor = new int[height][width];
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                rowMajor[i][j] = array[j][i];
            }
        }

        return rowMajor;
    }

    public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb){
        int[][] newArray = new int[a.length][a[0].length];                  // adding 2 row-major matrices together
        boolean sameSize = true;

        int[][] newa;
        int[][] newb;

        if (formata == true && formatb == true){                        //checking if both arrays are row-major
            if (a.length == b.length && a[0].length == b[0].length){
                sameSize = true;
            }
            else {
                System.out.println("Unable to add input matrices.");
                return null;
            }
        }
        else if(formata == false && formatb == true){
            a = translate(a);
        }
        else if(formata == true && formatb == false){
            b = translate(b);
        }
        else{
            a = translate(a);
            b = translate(b);
        }


        if (sameSize){

            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a[0].length; j++) {
                    newArray[i][j] = a[i][j] + b[i][j];
                }
            }
        }

        return newArray;
    }

    public static void main (String args[]){
        Random rand = new Random();

        int width1 = rand.nextInt(5) + 1;
        int width2 = rand.nextInt(5) + 1;

        int height1 = rand.nextInt(5) + 1;
        int height2 = rand.nextInt(5) + 1;

        int[][] a = increasingMatrix(width1, height1, true);
        int[][] b = increasingMatrix(width1, height1, false);
        int[][] c = increasingMatrix(width2, height2, true);

        System.out.println("Generating a matrix with width " + width1 + " and height " + height1 + ":");
        printMatrix(a, true);
        System.out.println("");

        boolean formatb = false;
        System.out.println("Generating a matrix with width " + width1 + " and height " + height1 + ":");
        printMatrix(b, formatb);
        System.out.println("");
        
        System.out.println("Generating a matrix with width " + width2 + " and height " + height2 + ":");
        printMatrix(c, true);
        System.out.println("");

        System.out.println("Adding two matrices.");
        System.out.println("");
        printMatrix(a, true);
        System.out.println("");
        System.out.println("plus");
        printMatrix(b, formatb);
        System.out.println("");
        
        if(formatb){
            System.out.println("No need to translate to row major");
        }
        else{
            System.out.println("Translating column major to row major input");
        }

        System.out.println("output:");
        int[][] sumab = addMatrix(a, true, b, false);
        printMatrix(sumab, true);

        int[][] sumac = addMatrix(a, true, c, true);



    }
}